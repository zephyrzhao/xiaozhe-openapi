package test;

import com.alibaba.fastjson.JSONObject;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.xiaozhe.test.LoggingInterceptor;
import com.xiaozhe.test.constant.CallTaskType;
import com.xiaozhe.test.model.BaseParams;
import com.xiaozhe.test.model.CallPhoneModel;
import com.xiaozhe.test.model.CallTaskModel;
import com.xiaozhe.test.model.CallTaskQuery;
import com.xiaozhe.test.model.CallTaskUpdate;
import com.xiaozhe.test.model.CustomerInfoExtVO;
import com.xiaozhe.test.model.EnterpriseEnumDTO;
import com.xiaozhe.test.model.ImportTaskCustomerVO;
import com.xiaozhe.test.model.PageResult;
import com.xiaozhe.test.model.SpeechModel;
import com.xiaozhe.test.model.TaskCreateInfoModel;
import com.xiaozhe.test.util.SignUtils;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.util.CollectionUtils;
import org.springframework.util.ObjectUtils;
import org.springframework.web.client.RestTemplate;

@Slf4j
public class XZAITest {

  private static final String APP_KEY = "企业的API Key";

  private static final String APP_SECRET = "企业的Secret Key";

  private static final String REQUEST_PRE = "http://thirdcall.zhexinit.com/openapi/v1";

  /**
   * 获取企业信息，企业ID
   */
  @Test
  public void getEnterprises() {
    HttpHeaders headers = getHttpHeaders();
    HttpEntity<String> entity = new HttpEntity<>(headers);
    RestTemplate restTemplate = new RestTemplate();
    restTemplate.setInterceptors(Collections.singletonList(new LoggingInterceptor()));
    BaseParams baseParams = new BaseParams(System.currentTimeMillis() / 1000 + "",
        System.currentTimeMillis() + "", "");
    baseParams.setSignature(getSignStr(baseParams));
    ObjectMapper objectMapper = new ObjectMapper();
    Map map = objectMapper.convertValue(baseParams, Map.class);
    ResponseEntity exchange = restTemplate
        .exchange(
            REQUEST_PRE
                + "/enterprise/getEnterprises?timestamp={timestamp}&nonce={nonce}&signature={signature}",
            HttpMethod.GET, entity, EnterpriseEnumDTO.class, map);
    log.info("请求结果为[{}]", exchange);
  }

  /**
   * 获取企业外线信息
   */
  @Test
  public void getPhoneList() {
    HttpHeaders headers = getHttpHeaders();
    RestTemplate restTemplate = new RestTemplate();
    restTemplate.setInterceptors(Collections.singletonList(new LoggingInterceptor()));
    HttpEntity<String> entity = new HttpEntity<>(headers);
    BaseParams baseParams = new BaseParams(System.currentTimeMillis() / 1000 + "",
        System.currentTimeMillis() + "", "");
    baseParams.setSignature(getSignStr(baseParams));
    ObjectMapper objectMapper = new ObjectMapper();
    Map map = objectMapper.convertValue(baseParams, Map.class);
    ResponseEntity exchange = restTemplate
        .exchange(
            REQUEST_PRE
                + "/enterprise/getPhoneList?timestamp={timestamp}&nonce={nonce}&signature={signature}",
            HttpMethod.GET, entity, new ParameterizedTypeReference<List<CallPhoneModel>>() {
            }, map);
    log.info("请求结果为[{}]", exchange);

  }

  /**
   * 获取话术列表
   */
  @Test
  public void getSpeechList() {
    HttpHeaders headers = getHttpHeaders();
    RestTemplate restTemplate = new RestTemplate();
    restTemplate.setInterceptors(Collections.singletonList(new LoggingInterceptor()));
    HttpEntity<String> entity = new HttpEntity<>(headers);
    BaseParams baseParams = new BaseParams(System.currentTimeMillis() / 1000 + "",
        System.currentTimeMillis() + "", "");
    baseParams.setSignature(getSignStr(baseParams));
    ObjectMapper objectMapper = new ObjectMapper();
    Map map = objectMapper.convertValue(baseParams, Map.class);
    ResponseEntity exchange = restTemplate
        .exchange(
            REQUEST_PRE
                + "/enterprise/getSpeechListByEnterpriseId?timestamp={timestamp}&nonce={nonce}&signature={signature}",
            HttpMethod.GET, entity, new ParameterizedTypeReference<List<SpeechModel>>() {
            }, map);
    log.info("请求结果为[{}]", exchange);
  }

  /**
   * 获取创建任务的初始化信息
   */
  @Test
  public void getTaskCreateInfo() {
    HttpHeaders headers = getHttpHeaders();
    RestTemplate restTemplate = new RestTemplate();
    restTemplate.setInterceptors(Collections.singletonList(new LoggingInterceptor()));
    HttpEntity<String> entity = new HttpEntity<>(headers);
    BaseParams baseParams = new BaseParams(System.currentTimeMillis() / 1000 + "",
        System.currentTimeMillis() + "", "");
    baseParams.setSignature(getSignStr(baseParams));
    ObjectMapper objectMapper = new ObjectMapper();
    Map map = objectMapper.convertValue(baseParams, Map.class);
    ResponseEntity exchange = restTemplate
        .exchange(REQUEST_PRE + "/task/task_create_info?timestamp={timestamp}&nonce={nonce}&signature={signature}",
            HttpMethod.GET, entity, TaskCreateInfoModel.class, map);
    log.info("请求结果为[{}]", exchange.getBody().toString());

  }

  /**
   * 创建初始化任务对象
   */
  public TaskCreateInfoModel getTaskCreateInfoModel() {
    HttpHeaders headers = getHttpHeaders();
    RestTemplate restTemplate = new RestTemplate();
    restTemplate.setInterceptors(Collections.singletonList(new LoggingInterceptor()));
    HttpEntity<String> entity = new HttpEntity<>(headers);
    BaseParams baseParams = new BaseParams(System.currentTimeMillis() / 1000 + "",
        System.currentTimeMillis() + "", "");
    baseParams.setSignature(getSignStr(baseParams));
    ObjectMapper objectMapper = new ObjectMapper();
    Map map = objectMapper.convertValue(baseParams, Map.class);
    ResponseEntity<TaskCreateInfoModel> exchange = restTemplate.exchange(REQUEST_PRE
                + "/task/task_create_info?timestamp={timestamp}&nonce={nonce}&signature={signature}",
            HttpMethod.GET, entity, TaskCreateInfoModel.class, map);
    log.info("请求结果为[{}]", exchange);
    return exchange.getBody();
  }

  /**
   * 获取任务列表
   */
  @Test
  public void getTaskList() {
    HttpHeaders headers = getHttpHeaders();
    RestTemplate restTemplate = new RestTemplate();
    restTemplate.setInterceptors(Collections.singletonList(new LoggingInterceptor()));
    HttpEntity<String> entity = new HttpEntity<>(headers);
    CallTaskQuery callTaskQuery = CallTaskQuery.builder().page(1L).pageSize(10L)
        .timestamp(System.currentTimeMillis() / 1000 + "")
        .nonce(System.currentTimeMillis() + "").build();

    callTaskQuery.setSignature(getSignStr(callTaskQuery));
    ObjectMapper objectMapper = new ObjectMapper();
    Map map = objectMapper.convertValue(callTaskQuery, Map.class);
    ResponseEntity exchange = restTemplate
        .exchange(REQUEST_PRE + "/task/getTasks?page={page}&pageSize={pageSize}&timestamp={timestamp}&nonce={nonce}&signature={signature}",
            HttpMethod.GET, entity, PageResult.class, map);
    log.info("请求结果为[{}]", exchange);
  }

  /**
   * 创建任务
   */
  @Test
  public void createTask() {
    // 1.获取创建任务需要的信息
    TaskCreateInfoModel taskCreateInfoModel = getTaskCreateInfoModel();
    if (taskCreateInfoModel == null) {
      log.error("获取任务需要创建的信息失败");
      return;
    }
    Integer aiNum = taskCreateInfoModel.getAiNum();
    if(ObjectUtils.isEmpty(aiNum) || aiNum <= 0){
      aiNum = 1;
    }

    List<CallPhoneModel> callPhone = taskCreateInfoModel.getCallPhone();
    List<SpeechModel> templateMap = taskCreateInfoModel.getTemplateMap();

    if(CollectionUtils.isEmpty(templateMap)){
      log.error("当前公司没有创建话术");
      return;
    }
    //从话术列表中任意选择话术
    SpeechModel speechModel = templateMap.get(0);

    if(CollectionUtils.isEmpty(callPhone)){
      log.error("当前公司的线路列表为空");
      return;
    }
    //从线路列表中任务选择线路
    CallPhoneModel callPhoneModel = callPhone.get(0);

    CallTaskModel callTaskModel = CallTaskModel.builder().name("三方调用测试任务名称-不可重复10")
        .aiCount(aiNum).callPhoneParams(Collections.singletonList(callPhoneModel))
        .callTaskType(CallTaskType.TTS_AI)
        .persistent(CallTaskType.PERSISTENT_TASK).emptyNumberSwitch(CallTaskType.EMPTYNUMBERSWITCH)
        .singleRecordSwitch(CallTaskType.NO_SINGLERECORDSWITCH)
        .templateCode(speechModel.getTemplateCode()).templateName(speechModel.getTemplateName())
        .timestamp(System.currentTimeMillis() / 1000 + "").nonce(System.currentTimeMillis() + "")
        .isReboot("YES")
        .rebootNum(3)
        .rebootStatus("2,3,4,5,6,7,8,9")
        .rebootPeriod(2)
        .build();

    HttpHeaders headers = getHttpHeaders();
    RestTemplate restTemplate = new RestTemplate();
    restTemplate.setInterceptors(Collections.singletonList(new LoggingInterceptor()));
    callTaskModel.setSignature(getPostSignStr(callTaskModel));
    ObjectMapper objectMapper = new ObjectMapper();
    Map map = objectMapper.convertValue(callTaskModel, Map.class);
    //post方
    HttpEntity<Map> entity = new HttpEntity<>(map, headers);
    ResponseEntity exchange = restTemplate.exchange(REQUEST_PRE + "/task/createTask",HttpMethod.POST, entity, Boolean.class);
    log.info("请求结果为[{}]", exchange);

  }

  /**
   * 号码导入
   */
  @Test
  public void importCustomerToTask() {
    HttpHeaders headers = getHttpHeaders();
    RestTemplate restTemplate = new RestTemplate();
    restTemplate.setInterceptors(Collections.singletonList(new LoggingInterceptor()));
    //添加手机号码
    List<CustomerInfoExtVO> customerInfoExtVOS = new ArrayList<>();
    CustomerInfoExtVO customerInfoExtVO = new CustomerInfoExtVO();
    customerInfoExtVO.setPhone("客户手机号码");
    customerInfoExtVOS.add(customerInfoExtVO);
    ImportTaskCustomerVO importTaskCustomerVO = ImportTaskCustomerVO.builder()
        //设置任务ID
        .callTaskId(222777303664304137L)
        .customerInfoList(customerInfoExtVOS).timestamp(System.currentTimeMillis() / 1000 + "")
        .nonce(System.currentTimeMillis() + "").build();
    importTaskCustomerVO.setSignature(getPostSignStr(importTaskCustomerVO));
    ObjectMapper objectMapper = new ObjectMapper();
    Map map = objectMapper.convertValue(importTaskCustomerVO, Map.class);
    HttpEntity<Map> entity = new HttpEntity<>(map, headers);
    ResponseEntity exchange = restTemplate.exchange(REQUEST_PRE+ "/task/importTaskCustomer",HttpMethod.POST, entity, Object.class);
    log.info("请求结果为[{}]", exchange);
  }

  @Test
  public void startTask() {
    HttpHeaders headers = getHttpHeaders();
    RestTemplate restTemplate = new RestTemplate();
    restTemplate.setInterceptors(Collections.singletonList(new LoggingInterceptor()));
    CallTaskUpdate callTaskUpdate = CallTaskUpdate.builder()
        .timestamp(System.currentTimeMillis() / 1000 + "").nonce(System.currentTimeMillis() + "")
        //替换为需要启动的任务ID
        .callTaskId(222777303664304137L).build();
    callTaskUpdate.setSignature(getSignStr(callTaskUpdate));
    ObjectMapper objectMapper = new ObjectMapper();
    Map map = objectMapper.convertValue(callTaskUpdate, Map.class);
    HttpEntity<Map> entity = new HttpEntity<>(map, headers);
    ResponseEntity exchange = restTemplate.exchange(REQUEST_PRE + "/task/start",HttpMethod.PUT, entity, Boolean.class);
    log.info("请求结果为[{}]", exchange);
  }

  @Test
  public void stopTask() {
    HttpHeaders headers = getHttpHeaders();
    RestTemplate restTemplate = new RestTemplate();
    restTemplate.setInterceptors(Collections.singletonList(new LoggingInterceptor()));
    CallTaskUpdate callTaskUpdate = CallTaskUpdate.builder()
        .timestamp(System.currentTimeMillis() / 1000 + "").nonce(System.currentTimeMillis() + "")
        //替换为需要启动的任务ID
        .callTaskId(220712558252900355L).build();
    callTaskUpdate.setSignature(getSignStr(callTaskUpdate));
    ObjectMapper objectMapper = new ObjectMapper();
    Map map = objectMapper.convertValue(callTaskUpdate, Map.class);
    HttpEntity<Map> entity = new HttpEntity<>(map, headers);
    ResponseEntity exchange = restTemplate.exchange(REQUEST_PRE + "/task/stop",HttpMethod.PUT, entity, Boolean.class);
    log.info("请求结果为[{}]", exchange);
  }


  private String getSignStr(Object baseParams) {
    ObjectMapper objectMapper = new ObjectMapper();
    Map map = objectMapper.convertValue(baseParams, Map.class);
    Set set = map.keySet();
    //参数校验
    StringBuffer sb = new StringBuffer();
    set.stream().sorted().forEach(key -> {
      Object value = map.get(key);
      if (!"signature".equals(key) && value != null) {
        sb.append(key).append("=").append(JSONObject.toJSON(value)).append("&");
      }
    });
    String substring = sb.toString().substring(0, sb.length() - 1);
    log.info("参数为[{}]", substring);
    return SignUtils.parameterSha1(substring);
  }

  private String getPostSignStr(Object baseParams) {
    ObjectMapper objectMapper = new ObjectMapper();
    Map map = objectMapper.convertValue(baseParams, Map.class);
    Set set = map.keySet();
    //参数校验
    StringBuffer sb = new StringBuffer();
    set.stream().sorted().forEach(key -> {
      Object value = map.get(key);
      if (!"signature".equals(key)) {
        sb.append(key).append("=").append(JSONObject.toJSON(value)).append("&");
      }
    });
    String substring = sb.toString().substring(0, sb.length() - 1);
    log.info("参数为[{}]", substring);
    return SignUtils.parameterSha1(substring);
  }

  private HttpHeaders getHttpHeaders() {
    HttpHeaders httpHeaders = new HttpHeaders();
    //填写你的APP_KEY,APP_SECRET
    httpHeaders.add("APP_KEY", APP_KEY);
    httpHeaders.add("APP_SECRET", APP_SECRET);
    httpHeaders.add("Content-Type", "application/json");
    return httpHeaders;
  }

}
