package com.xiaozhe.test.model;

import java.util.Date;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
public class CallTaskModel extends BaseParams {

  /**
   * 呼叫任务名称,所属表字段为call_task.name,不能为空
   */
  private String name;

  /**
   * 话术模板code,template_code
   */
  private String templateCode;

  /**
   * 话术模板名称,template_name
   */
  private String templateName;

  /**
   * 任务开始时间,所属表字段为call_task.start_time
   */
  private Date startTime;

  /**
   * 任务结束时间,所属表字段为call_task.end_time
   */
  private Date endTime;

  /**
   * AI数量,所属表字段为call_task.ai_count
   */
  private Integer aiCount;

  /**
   * 主叫号码集合,即外线
   */
  private List<CallPhoneModel> callPhoneParams;


  /**
   * 任务描述
   */
  private String remark;

  /**
   * 自动拨打并发
   */
  private Double concurrentRate;

  /**
   * 任务类型 TTS_AI
   */
  private String callTaskType;

  /**
   * 任务支持永久有效 "YES":持久有效 "NO":设置时间
   */
  private String persistent;
  /**
   * 是否开启自动重启 "YES":开启 “NO”:不开启
   */
  private String isReboot;
  /**
   * 重复拨打的状态
   */
  private String rebootStatus;
  /**
   * 重复拨打次数
   */
  private Integer rebootNum;
  /**
   * 重复拨打时间
   */
  private Integer rebootPeriod;
  /**
   * 空号识别开关
   */
  private Integer emptyNumberSwitch;
  /**
   * 单个录音上传
   */
  private Integer singleRecordSwitch;

  @Builder
  public CallTaskModel(String timestamp, String nonce, String signature, String name,
      String templateCode, String templateName, Date startTime, Date endTime,
      Integer aiCount, List<CallPhoneModel> callPhoneParams, String remark,
      Double concurrentRate, String callTaskType, String persistent, String isReboot,
      String rebootStatus, Integer rebootNum, Integer rebootPeriod,
      Integer emptyNumberSwitch, Integer singleRecordSwitch) {
    super(timestamp, nonce, signature);
    this.name = name;
    this.templateCode = templateCode;
    this.templateName = templateName;
    this.startTime = startTime;
    this.endTime = endTime;
    this.aiCount = aiCount;
    this.callPhoneParams = callPhoneParams;
    this.remark = remark;
    this.concurrentRate = concurrentRate;
    this.callTaskType = callTaskType;
    this.persistent = persistent;
    this.isReboot = isReboot;
    this.rebootStatus = rebootStatus;
    this.rebootNum = rebootNum;
    this.rebootPeriod = rebootPeriod;
    this.emptyNumberSwitch = emptyNumberSwitch;
    this.singleRecordSwitch = singleRecordSwitch;
  }
}
