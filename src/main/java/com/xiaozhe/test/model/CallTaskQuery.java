package com.xiaozhe.test.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

public class CallTaskQuery extends BaseParams {

  /**
   * 任务状态：1-未启动，2-进行中，3-已停止，4-已完成
   */
  private Integer status;

  /**
   * 呼叫任务名称
   */
  private String callTaskName;

  private Long page;

  private Long pageSize;


  @Builder
  CallTaskQuery(String  timestamp, String nonce, String signature,Integer status,String  callTaskName,Long page,Long pageSize) {
    super(timestamp, nonce, signature);
    this.callTaskName = callTaskName;
    this.status = status;
    this.page = page;
    this.pageSize = pageSize;
  }

  public Integer getStatus() {
    return status;
  }

  public void setStatus(Integer status) {
    this.status = status;
  }

  public String getCallTaskName() {
    return callTaskName;
  }

  public void setCallTaskName(String callTaskName) {
    this.callTaskName = callTaskName;
  }

  public Long getPage() {
    return page;
  }

  public void setPage(Long page) {
    this.page = page;
  }

  public Long getPageSize() {
    return pageSize;
  }

  public void setPageSize(Long pageSize) {
    this.pageSize = pageSize;
  }
}
