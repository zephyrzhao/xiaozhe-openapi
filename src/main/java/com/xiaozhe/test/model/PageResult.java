package com.xiaozhe.test.model;

import java.util.List;
import lombok.Data;

@Data
public class PageResult<T> {

  /**
   * 当前页数
   */
  private Integer page;

  /**
   * 页面显示条数
   */
  private Integer pageSize;

  // 总条数
  private Integer total;

  // 具体数据
  private List<T> list;

}
