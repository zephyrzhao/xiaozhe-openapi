package com.xiaozhe.test.model;

import java.util.List;
import lombok.Data;


@Data
public class TaskCreateInfoModel {

  /**
   * 主叫号码集合
   */
  private List<CallPhoneModel> callPhone;
  /**
   * 话术模板Map
   */
  private List<SpeechModel> templateMap;
  /**
   * AI 总数
   */
  private Integer aiNum;
  /**
   * 剩余AI数量
   */
  private Integer leftAiNum;
  /**
   * 是否开启自动重启 "YES":开启 “NO”:不开启
   */
  private String isReboot;

  private Long rebootPeriod;

  /**
   * 重复拨打的状态
   */
  private String rebootStatus;
  /**
   * 重复拨打次数
   */
  private Integer rebootNum;


  @Override
  public String toString() {
    return "{" +
        "callPhone=" + callPhone +
        ", templateMap=" + templateMap +
        ", aiNum=" + aiNum +
        ", leftAiNum=" + leftAiNum +
        '}';
  }
}
