package com.xiaozhe.test.model;

import java.util.Date;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;


@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class CallTaskVO {

  /**
   * 呼叫任务主键,所属表字段为call_task.call_task_id
   */
  private Long callTaskId;

  /**
   * callTaskType (person: 人工外呼，AI: AI外呼)
   */
  private String callTaskType;

  /**
   * 企业ID,所属表字段为call_task.enterprise_id
   */
  private Long enterpriseId;

  /**
   * 任务创建者ID,所属表字段为call_task.user_id
   */
  private Long userId;


  /**
   * 呼叫任务名称,所属表字段为call_task.name
   */
  private String name;

  /**
   * 话术模板code,所属表字段为call_task.template_code
   */
  private String templateCode;

  /**
   * 话术模板名称,所属表字段为call_task.template_name
   */
  private String templateName;

  /**
   * 主叫号码信息
   */
  private String callPhoneParams;

  /**
   * 任务开始时间,所属表字段为call_task.start_time
   */
  private Date startTime;

  /**
   * 任务结束时间,所属表字段为call_task.end_time
   */
  private Date endTime;

  /**
   * 任务状态：1未启动，2进行中，3停止中，4已完成,所属表字段为call_task.status
   */
  private Byte status;

  /**
   * 任务描述,所属表字段为call_task.remark
   */
  private String remark;

  /**
   * 任务完成时间,所属表字段为call_task.complete_time
   */
  private Date completeTime;

  /**
   * AI数量,所属表字段为call_task.ai_count
   */
  private Integer aiCount;

  /**
   * 任务总数量
   */
  private Long sumCount;

  /**
   * 已经完成的数量
   */
  private Long finishCount;

  /**
   * 创建时间
   */
  private Date createTime;

  /**
   * 创建人
   */
  private String createName;

  /**
   * 接通率
   */
  private String callRate;

  /**
   * 比率
   */
  private Double concurrentRate;



}
