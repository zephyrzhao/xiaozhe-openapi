package com.xiaozhe.test.model;

import java.util.Date;
import lombok.Data;

/**
 * 话术
 */
@Data
public class SpeechModel {

  /**
   * 话术id ( 主键 )
   */
  private Long speechId;
  /**
   * 话术名称
   */
  private String templateName;
  /**
   * 话术编号
   */
  private String templateCode;
  /**
   * 更新时间
   */
  private Date modifyTime;
  /**
   * 所属行业
   */
  private String industryName;

  @Override
  public String toString() {
    return "{" +
        "speechId=" + speechId +
        ", templateName='" + templateName + '\'' +
        ", templateCode='" + templateCode + '\'' +
        ", modifyTime=" + modifyTime +
        ", industryName='" + industryName + '\'' +
        '}';
  }
}
