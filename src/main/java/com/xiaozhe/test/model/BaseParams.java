package com.xiaozhe.test.model;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class BaseParams {

  /**
   * 当前时间，秒时间戳
   */
  private String timestamp;
  /**
   * 随机数
   */
  private String nonce;
  /**
   * 加密参数
   */
  private String signature;

}
