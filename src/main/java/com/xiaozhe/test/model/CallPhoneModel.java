package com.xiaozhe.test.model;

import lombok.Data;

/**
 * @author pjk
 * @date 2018-12-20 14:39
 * @since
 */
@Data
public class CallPhoneModel {

  /**
   * 主叫号码ID
   */
  private Long callPhoneId;
  /**
   * 主叫号码
   */
  private String callPhoneNumber;
  /**
   * 占用状态 0:空闲 1：占用
   */
  private Integer status;

  @Override
  public String toString() {
    return "{" +
        "callPhoneId=" + callPhoneId +
        ", callPhoneNumber='" + callPhoneNumber + '\'' +
        ", status=" + status +
        '}';
  }
}
