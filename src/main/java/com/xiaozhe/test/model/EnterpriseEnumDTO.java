package com.xiaozhe.test.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * 类名:EnterpriseEnumDTO <pre>
 * 描述: 企业列表
 * 编写者:zephyr
 * @create 2018-12-14 14:22
 * </pre>
 */
@Getter
@Setter
@ToString
public class EnterpriseEnumDTO {

  private Long entId;

  /**
   * 企业名称
   */
  private String entName;

}
