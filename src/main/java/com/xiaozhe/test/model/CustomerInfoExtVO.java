package com.xiaozhe.test.model;

import java.util.HashMap;
import java.util.Map;
import javax.validation.constraints.NotEmpty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 客户信息实体类
 **/
@Data
@NoArgsConstructor
@AllArgsConstructor
public class CustomerInfoExtVO {


  /**
   * 姓名
   */
  private String name;

  /**
   * 手机号
   */
  @NotEmpty(message = "手机号不能为空")
  private String phone;

  /**
   * 年龄,所属表字段为customer.age
   */
  private Integer age;

  /**
   * 公司名称,所属表字段为customer.company_name
   */
  private String companyName;

  /**
   * 地区地址,所属表字段为customer.address
   */
  private String address;

  /**
   * 客户邮箱,所属表字段为customer.email
   */
  private String email;

  /**
   * 客户/线索来源：文本,所属表字段为customer.source
   */
  private String source;

  /**
   * 客户额外信息，如果话术中包含变量，则需要在properties中加上此字段，map的key和话术中的变量保持一致
   */
  private Map<String, Object> properties = new HashMap<>();


}
