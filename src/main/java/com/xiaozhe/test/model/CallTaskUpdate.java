package com.xiaozhe.test.model;

import lombok.Builder;

public class CallTaskUpdate extends BaseParams {

  private Long callTaskId;

  @Builder
  public CallTaskUpdate(String timestamp, String nonce, String signature,Long callTaskId) {
    super(timestamp, nonce, signature);
    this.callTaskId = callTaskId;
  }

  public Long getCallTaskId() {
    return callTaskId;
  }

  public void setCallTaskId(Long callTaskId) {
    this.callTaskId = callTaskId;
  }
}
