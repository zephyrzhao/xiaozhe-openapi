package com.xiaozhe.test.model;


import java.util.List;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 向任务中导入客户实体类
 */
@Data
public class ImportTaskCustomerVO extends BaseParams{

  /**
   * 任务id
   */
  private Long callTaskId;

  /**
   * 导入的客户信息
   */
  private List<CustomerInfoExtVO> customerInfoList;

  /**
   * API指定的回调地址
   */
  private String callBackUrl;


  @Builder
  public ImportTaskCustomerVO(String timestamp, String nonce, String signature,
      Long callTaskId, List<CustomerInfoExtVO> customerInfoList, String callBackUrl) {
    super(timestamp, nonce, signature);
    this.callTaskId = callTaskId;
    this.customerInfoList = customerInfoList;
    this.callBackUrl = callBackUrl;
  }
}
