package com.xiaozhe.test.model;

import lombok.Data;

/**
 * @author pjk
 * @date 2019-04-01 17:00
 * @since
 */
@Data
public class SpeechTemplateDTO {

  private String templateCode;

  private String templateName;

  private Integer isTransfer;
}
