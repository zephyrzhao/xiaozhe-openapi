package com.xiaozhe.test.util;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * 参数、签名验证
 */
public class SignUtils {

  /**
   * 参数按 A-Z进行排序
   *
   * @param params 请求的参数
   * @return 排序后的list
   */
  public static List<String> parameterSort(List<String> params) {
    Collections.sort(params);
    return params;
  }

  /**
   * 对参数进行拼接
   *
   * @param list 参数名排序后的list
   * @param param 传入的参数map
   * @return 拼接后的字符串
   */
  public static String getJoinParameter(List<String> list, Map<String, Object> param) {
    return list.stream().map((key) -> key + "=" + param.get(key)).collect(Collectors.joining("&"));
  }

  /**
   * 将字符串进行sha1加密
   *
   * @param parameter 需要加密的字符串
   * @return 加密后的内容
   */
  public static String parameterSha1(String parameter) {
    try {
      MessageDigest digest = MessageDigest.getInstance("SHA-1");
      digest.update(parameter.getBytes());
      byte[] messageDigest = digest.digest();
      // Create Hex String
      StringBuffer hexString = new StringBuffer();
      // 字节数组转换为 十六进制 数
      for (int i = 0; i < messageDigest.length; i++) {
        String shaHex = Integer.toHexString(messageDigest[i] & 0xFF);
        if (shaHex.length() < 2) {
          hexString.append(0);
        }
        hexString.append(shaHex);
      }
      return hexString.toString();

    } catch (NoSuchAlgorithmException e) {
      e.printStackTrace();
    }
    return "";
  }

}
