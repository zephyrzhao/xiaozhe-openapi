package com.xiaozhe.test;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author 赵志锋
 * @description 启动类
 */
@SpringBootApplication
public class XiaoZheTestApplication {

  public static void main(String[] args) {
    SpringApplication.run(XiaoZheTestApplication.class, args);
  }

}
