package com.xiaozhe.test.constant;

public class CallTaskType {

  /**
   * 任务类型，目前仅支持TTS_AI。即AI外呼类型
   */
  public static final String TTS_AI = "TTS_AI";
  /**
   * 是不限时的任务类型
   */
  public static final String PERSISTENT_TASK = "YES";
  /**
   * 非不限时的任务类型，如果选择该类型，需要传入任务的开始，结束时间
   */
  public static final String NOT_PERSISTENT_TASK = "NO";
  /**
   * 空号识别开关
   */
  public static final Integer EMPTYNUMBERSWITCH = 1;

  public static final Integer NOT_EMPTYNUMBERSWITCH = 0;
  /**
   * 单句话切片开关
   */
  public static final Integer SINGLERECORDSWITCH = 1;

  public static final Integer NO_SINGLERECORDSWITCH = 0;
}
